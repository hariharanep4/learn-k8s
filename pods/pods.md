kubectl run redis --image=redis123 --dry-run=client -o yaml > redis-pod-definition.yaml

kubectl create -f redis-pod-definition.yaml

kubectl get pods

kubectl edit pod redis

kubectl apply -f redis-pod-definition.yaml

kubectl run nginx --image=nginx --dry-run=client -o yaml > nginx-pod-definition.yaml

kubectl create -f nginx-pod-definition.yaml

kubectl describe pod <pod_name> Example:  kubectl describe pod newpods-8cn69

kubectl get pods -o wide

READY
1/1   - Running containers in POD/Total container in POD.

kubectl delete pod <pod_name> Example: kubectl delete pod webapp

