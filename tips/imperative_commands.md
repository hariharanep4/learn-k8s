kubectl run redis --image=redis:alpine -l tier=db

kubectl expose pod redis --port=6739 --name redis-service

kubectl create deployment webapp --image=kodekloud/webapp-color --replicas=3

kubectl run custom-nginx --image=nginx --port=8080

kubectl create ns dev-ns OR kubectl create namespace dev-ns

kubectl create deployment redis-deploy --image=redis --replicas=2 -n dev-ns

kubectl run httpd --image=httpd:alpine --port=80 --expose

kubectl get pods --selector env=dev --no-headers | wc -l

kubectl get all --selector env=prod --no-headers | wc -l