# To taint the node

kubectl taint node node01 spray=mortein:NoSchedule

# To untaint the node

kubectl taint node node01 spray=mortein:NoSchedule-

kubectl taint nodes controlplane node-role.kubernetes.io/control-plane:NoSchedule-

# Labeling the node

kubectl label node node01 color=blue