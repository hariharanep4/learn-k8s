kubectl get replicasets

ReplicaSet ensures that desired number of PODs always run.

kubectl delete replicaset <replicaset_name> Example: kubectl delete replicaset replicaset-1 replicaset-2

kubectl delete -f <replicaset_definition_file_name>.yaml

kubectl edit replicaset new-replica-set

kubectl scale replicaset new-replica-set --replicas=5

